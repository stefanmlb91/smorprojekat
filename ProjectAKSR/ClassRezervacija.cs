﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAKSR
{
    class ClassRezervacija
    {
        public ClassRezervacija(string rezervacijaID, DateTime kreiranoDana, int brSedista, int klasa, string jmbg, string letID)
        {
            this.rezervacijaID = rezervacijaID;
            this.kreiranoDana = kreiranoDana;
            this.brSedista = brSedista;
            this.klasa = klasa;
            this.jmbg = jmbg;
            this.letID = letID;
        }

        public string rezervacijaID { get; set; }
        public DateTime kreiranoDana { get; set; }
        public int brSedista { get; set; }
        public int klasa { get; set; }
        public string jmbg { get; set; }
        public string letID { get; set; }
    }
}
