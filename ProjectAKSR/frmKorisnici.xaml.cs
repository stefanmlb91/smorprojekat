﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectAKSR
{
    /// <summary>
    /// Interaction logic for frmKorisnici.xaml
    /// </summary>
    public partial class frmKorisnici : Window
    {
        System.Windows.Threading.DispatcherTimer timMakeEditable = new System.Windows.Threading.DispatcherTimer();
        AKSREntities context = new AKSREntities();
        bool isInsertMode = false;
        bool isBeingEdited = false;

        public frmKorisnici()
        {
            InitializeComponent();
            ucitajKorisnike();
        }

        private void ucitajKorisnike()
        {
            dgKorisnici.ItemsSource = GetKorisnikList();
        }

        private ObservableCollection<korisnici> GetKorisnikList()
        {
            var list = from e in context.korisnicis select e;
            return new ObservableCollection<korisnici>(list);
        }

        private void dgEmp_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            korisnici korisnik = new korisnici();
            korisnici kor = e.Row.DataContext as korisnici;

            if (isInsertMode)
            {
                var InsertRecord = MessageBox.Show("Zelite li da dodate " + kor.ime + " kao novog korisnika?", "Potvrdi", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (InsertRecord == MessageBoxResult.Yes)
                {
                    korisnik.jmbg = kor.jmbg;
                    korisnik.ime = kor.ime;
                    korisnik.prezime = kor.prezime;
                    korisnik.status = kor.status;
                    korisnik.privilegija = kor.privilegija;
                    korisnik.datum_rodjenja = kor.datum_rodjenja;
                    korisnik.username = kor.username;
                    korisnik.email = kor.email;
                    korisnik.password = kor.password;

                    context.korisnicis.Add(korisnik);

                    try
                    {
                        context.SaveChanges();
                        MessageBox.Show(korisnik.ime + " " + korisnik.prezime + " je uspesno dodat!", "Dodavanja korisnika", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        MessageBox.Show("Greska prilikom upisa.");
                    }

                    ucitajKorisnike();
                }
                else
                    ucitajKorisnike();
            }
            else if (isBeingEdited)
            {
                var InsertRecord = MessageBox.Show("Zelite li da promenite podatke o korisniku: " + kor.ime + "?", "Potvrdi", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (InsertRecord == MessageBoxResult.Yes)
                {
                    try
                    {
                        context.SaveChanges();
                        MessageBox.Show("Podaci uspesno izmenjeni.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        MessageBox.Show("Greska prilikom izmene.");
                    }
                }
            }
        }

        private void dgEmp_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && !isBeingEdited)
            {
                var grid = (DataGrid)sender;
                if (grid.SelectedItems.Count > 0)
                {
                    var Res = MessageBox.Show("Da li ste sigurni da zelite da obrisete korisnika?", "Brisanje zapisa", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                    if (Res == MessageBoxResult.Yes)
                    {
                        foreach (var row in grid.SelectedItems)
                        {
                            korisnici korisnik = row as korisnici;
                            context.korisnicis.Remove(korisnik);
                        }
                        context.SaveChanges();
                        MessageBox.Show("Korisnik je obrisan!");
                    }
                    else
                        dgKorisnici.ItemsSource = GetKorisnikList();
                }
            }
        }

        private void dgEmp_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            isInsertMode = true;
        }

        private void dgEmp_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            isBeingEdited = true;
        }

        private void buttonZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            frmRegistracija dodajKorisnika = new frmRegistracija();
            dodajKorisnika.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            dodajKorisnika.Closed += ChildWindowClosed;
            dodajKorisnika.ShowDialog();
        }

        private void ChildWindowClosed(object sender, EventArgs e)
        {
            ((Window)sender).Closed -= ChildWindowClosed;
            ucitajKorisnike();
        }

        private void dgKorisnici_PreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs e)
        {
            /*
            if (isBeingEdited)
            {
                dgKorisnici.Columns[0].IsReadOnly = false;
            }
            else
            {
                dgKorisnici.Columns[0].IsReadOnly = true;
            }
            */
        }

        /*
        private void dgKorisnici_PreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs e)
        {
            if (isBeingEdited) 
            {
                timMakeEditable.Interval = new TimeSpan(0, 0, 0, 0, 100); // 100 Milliseconds 
                timMakeEditable.Tick += new EventHandler(timer_Tick);
                timMakeEditable.Start();

                dgKorisnici.Columns[0].IsReadOnly = true;
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            dgKorisnici.Columns[0].IsReadOnly = false;
            timMakeEditable.Stop();

        }
        */
    }
}
