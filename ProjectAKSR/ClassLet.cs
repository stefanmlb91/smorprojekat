﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAKSR
{
    class ClassLet
    {
        public ClassLet(string letID, string prevozilacID, string avionID, DateTime datumLeta, int status, string aerodromPolaska, string aerodromDolaska, ClassPrevozilac prevozilac)
        {
            this.letID = letID;
            this.avionID = avionID;
            this.datumLeta = datumLeta;
            this.status = status;
            this.aerodromPolaska = aerodromPolaska;
            this.aerodromDolaska = aerodromDolaska;
            this.prevozilacID = prevozilacID;
            this.prevozilac = prevozilac;
        }

        public string letID { get; set; }
        public string avionID { get; set; }
        public DateTime datumLeta { get; set; }
        public int status { get; set; }
        public string aerodromPolaska { get; set; }
        public string aerodromDolaska { get; set; }
        public string prevozilacID { get; set; }
        public ClassPrevozilac prevozilac { get; set; }
    }
}
