﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectAKSR
{
    /// <summary>
    /// Interaction logic for frmRezervisiLet.xaml
    /// </summary>
    public partial class frmRezervisiLet : Window
    {
        AKSREntities context = new AKSREntities();
        letovi let;

        public frmRezervisiLet()
        {
            InitializeComponent();
        }

        public frmRezervisiLet(letovi let)
        {
            InitializeComponent();
            this.let = let;
            pripremiFormu();
        }

        private void buttonUnesi_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbJMBG.Text) && ((bool)rb1.IsChecked || (bool)rb2.IsChecked) && !string.IsNullOrEmpty(tbBrojSedista.Text))
            {
                var InsertRecord = MessageBox.Show("Da li zelite da napravite rezervaciju za ovaj let?", "Potvrdi", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (InsertRecord == MessageBoxResult.Yes)
                {
                    APIController apiController = new APIController();
                    int klasa;
                    if ((bool)rb1.IsChecked)
                    {
                        klasa = 1;
                    }
                    else
                    {
                        klasa = 2;
                    }

                    string a = (string)labelBrojLeta.Content;
                    string b = tbBrojSedista.Text;
                    string c = tbJMBG.Text;
                    if (apiController.makeReservation(tbBrojSedista.Text, klasa.ToString(), tbJMBG.Text, a))
                    {
                        MessageBox.Show("Rezervacja uspesno kreirana!", "Rezervacija", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Greska prilikom kreiranja rezervacije.", "Greska", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Rezervacija uspesno napravljena.");
                }
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void pripremiFormu ()
        {
            labelBrojLeta.Content = let.let_id;
            labelOd.Content = let.aerodrom_polaska_id;
            labelDo.Content = let.aerodrom_dolaska_id;

            if (ClassKorisnik.privilegijaS == 1)
            {
                tbJMBG.IsEnabled = true;
                tbJMBG.Text = "";
            }
            else
            {
                tbJMBG.IsEnabled = false;
                tbJMBG.Text = ClassKorisnik.jmbgS;
            }
        }
    }
}
