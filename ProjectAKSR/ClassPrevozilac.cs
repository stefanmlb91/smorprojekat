﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAKSR
{
    class ClassPrevozilac
    {
        public ClassPrevozilac(string prevozilacID, string telefon, string ime, int status)
        {
            this.prevozilacID = prevozilacID;
            this.telefon = telefon;
            this.ime = ime;
            this.status = status;
        }

        public string prevozilacID { get; set; }
        public string telefon { get; set; }
        public string ime { get; set; }
        public int status { get; set; }
    }
}
