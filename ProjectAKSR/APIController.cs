﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ProjectAKSR
{
    class APIController
    {
        /*
        private static string SERVER_ADDRESS = "STEFAN-PC\\SQLEXPRESS";
        private static string DATABASE = "AKSR_STAGED";
        private static string PORT = "3306";
        private static string USERNAME = "1796141_aksr";
        private static string PASSWORD = "test";
        */

        private static string CONNECTION_STRING = "server=.\\SQLEXPRESS;initial catalog=AKSR_STAGED;trusted_connection=true";

        private static string EscapeConnectionStringValue(string value)
        {
            return value.Replace(";", "\";\"");
        }

        public string GetConnectionString()
        {
            return "server=localhost;initial catalog=newmits;trusted_connection=true";
        }

        public bool userAuth(string username, string password)
        {
            String query = "SELECT * FROM korisnici WHERE USERNAME = '" + username + "'AND PASSWORD = '" + password + "'";
            //String conString = "server=.\\SQLEXPRESS;initial catalog=AKSR_STAGED;trusted_connection=true";
            SqlConnection con = new SqlConnection(CONNECTION_STRING);
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dbr;
            try
            {
                con.Open();
                dbr = cmd.ExecuteReader();

                int count = 0;
                while (dbr.Read())
                {
                    count = count + 1;
                    Console.WriteLine(count);
                    Console.WriteLine("SUCCESS");

                    /*
                    Settings.Default["jmbg"] = dbr[0];
                    Settings.Default["ime"] = dbr[1];
                    Settings.Default["prezime"] = dbr[2];
                    Settings.Default["status"] = dbr[8];
                    Settings.Default["privilegija"] = dbr[3];
                    Settings.Default["datumRodjenja"] = dbr[4];
                    Settings.Default["korisnickoIme"] = dbr[5];
                    Settings.Default["email"] = dbr[7];
                    Settings.Default["loggedIn"] = true;
                    Settings.Default.Save();
                    */

                    ClassKorisnik.jmbgS = (string)dbr[0];
                    ClassKorisnik.imeS = (string)dbr[1];
                    ClassKorisnik.prezimeS = (string)dbr[2];
                    ClassKorisnik.statusS = (int)dbr[8];
                    ClassKorisnik.privilegijaS = (int)dbr[3];
                    ClassKorisnik.datumRodjenjaS = (DateTime)dbr[4];
                    ClassKorisnik.korisnickoImeS = (string)dbr[5];
                    ClassKorisnik.emailS = (string)dbr[7];
                }
                if (count == 1)
                {
                    Console.WriteLine("COUNT: '{0}", count);
                    return true;
                }
                else
                {
                    Console.WriteLine("FALSE");
                    return false;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
            }
            finally
            {
                Console.WriteLine("CLOSE CONNECTION");
                con.Close();
            }
            return false;
        }

        public bool createAccount(string ime, string prezime, string jmbg, string email, string username, string password, string dob)
        {
            SqlConnection sqlConn = new SqlConnection(CONNECTION_STRING);
            SqlCommand sqlComm = new SqlCommand();
            sqlComm = sqlConn.CreateCommand();
            sqlComm.CommandText = @"INSERT INTO korisnici (jmbg, ime, prezime, privilegija, datum_rodjenja, username, password, email, status) VALUES (@jmbg, @ime, @prezime, @privilegija, @datum_rodjenja, @username, @password, @email, @status)";
            sqlComm.Parameters.Add("@jmbg", SqlDbType.VarChar);
            sqlComm.Parameters["@jmbg"].Value = jmbg;
            sqlComm.Parameters.Add("@ime", SqlDbType.VarChar);
            sqlComm.Parameters["@ime"].Value = ime;
            sqlComm.Parameters.Add("@prezime", SqlDbType.VarChar);
            sqlComm.Parameters["@prezime"].Value = prezime;
            sqlComm.Parameters.Add("@privilegija", SqlDbType.Int);
            sqlComm.Parameters["@privilegija"].Value = 0;
            sqlComm.Parameters.Add("@datum_rodjenja", SqlDbType.Date);
            sqlComm.Parameters["@datum_rodjenja"].Value = dob;
            sqlComm.Parameters.Add("@username", SqlDbType.VarChar);
            sqlComm.Parameters["@username"].Value = username;
            sqlComm.Parameters.Add("@password", SqlDbType.VarChar);
            sqlComm.Parameters["@password"].Value = password;
            sqlComm.Parameters.Add("@email", SqlDbType.VarChar);
            sqlComm.Parameters["@email"].Value = email;
            sqlComm.Parameters.Add("@status", SqlDbType.Int);
            sqlComm.Parameters["@status"].Value = 1;

            System.Console.WriteLine("1*****");
            try
            {
                System.Console.WriteLine("2*****");
                sqlConn.Open();
                sqlComm.ExecuteNonQuery();
                return true;
            }
            catch
            {
                System.Console.WriteLine("3*****");
            }
            finally
            {
                sqlConn.Close();
            }

            return false;
        }

        public bool createAccountAdmin(string ime, string prezime, string jmbg, string email, string username, string password, string dob, int privilegija)
        {
            SqlConnection sqlConn = new SqlConnection(CONNECTION_STRING);
            SqlCommand sqlComm = new SqlCommand();
            sqlComm = sqlConn.CreateCommand();
            sqlComm.CommandText = @"INSERT INTO korisnici (jmbg, ime, prezime, privilegija, datum_rodjenja, username, password, email, status) VALUES (@jmbg, @ime, @prezime, @privilegija, @datum_rodjenja, @username, @password, @email, @status)";
            sqlComm.Parameters.Add("@jmbg", SqlDbType.VarChar);
            sqlComm.Parameters["@jmbg"].Value = jmbg;
            sqlComm.Parameters.Add("@ime", SqlDbType.VarChar);
            sqlComm.Parameters["@ime"].Value = ime;
            sqlComm.Parameters.Add("@prezime", SqlDbType.VarChar);
            sqlComm.Parameters["@prezime"].Value = prezime;
            sqlComm.Parameters.Add("@privilegija", SqlDbType.Int);
            sqlComm.Parameters["@privilegija"].Value = privilegija;
            sqlComm.Parameters.Add("@datum_rodjenja", SqlDbType.Date);
            sqlComm.Parameters["@datum_rodjenja"].Value = dob;
            sqlComm.Parameters.Add("@username", SqlDbType.VarChar);
            sqlComm.Parameters["@username"].Value = username;
            sqlComm.Parameters.Add("@password", SqlDbType.VarChar);
            sqlComm.Parameters["@password"].Value = password;
            sqlComm.Parameters.Add("@email", SqlDbType.VarChar);
            sqlComm.Parameters["@email"].Value = email;
            sqlComm.Parameters.Add("@status", SqlDbType.Int);
            sqlComm.Parameters["@status"].Value = 1;

            try
            {
                sqlConn.Open();
                sqlComm.ExecuteNonQuery();
                return true;
            }
            catch
            {

            }
            finally
            {
                sqlConn.Close();
            }

            return false;
        }

        public bool createFlight(string letID, string prevozilacID, string avionID, string aerodromPolaska, string AerodromDolaska, string datumLeta)
        {
            SqlConnection sqlConn = new SqlConnection(CONNECTION_STRING);
            SqlCommand sqlComm = new SqlCommand();
            sqlComm = sqlConn.CreateCommand();
            sqlComm.CommandText = @"INSERT INTO letovi (let_id, prevozilac_id, avion_id, aerodrom_dolaska_id, aerodrom_polaska_id, datum_leta, status) VALUES (@let_id, @prevozilac_id, @avion_id, @aerodrom_dolaska_id, @aerodrom_polaska_id, @datum_leta, @status)";
            sqlComm.Parameters.Add("@let_id", SqlDbType.VarChar);
            sqlComm.Parameters["@let_id"].Value = letID;
            sqlComm.Parameters.Add("@prevozilac_id", SqlDbType.VarChar);
            sqlComm.Parameters["@prevozilac_id"].Value = prevozilacID;
            sqlComm.Parameters.Add("@avion_id", SqlDbType.VarChar);
            sqlComm.Parameters["@avion_id"].Value = avionID;
            sqlComm.Parameters.Add("@aerodrom_dolaska_id", SqlDbType.VarChar);
            sqlComm.Parameters["@aerodrom_dolaska_id"].Value = AerodromDolaska;
            sqlComm.Parameters.Add("@aerodrom_polaska_id", SqlDbType.VarChar);
            sqlComm.Parameters["@aerodrom_polaska_id"].Value = aerodromPolaska;
            sqlComm.Parameters.Add("@datum_leta", SqlDbType.DateTime);
            sqlComm.Parameters["@datum_leta"].Value = datumLeta;
            sqlComm.Parameters.Add("@status", SqlDbType.Int);
            sqlComm.Parameters["@status"].Value = 1;

            try
            {
                sqlConn.Open();
                sqlComm.ExecuteNonQuery();
                return true;
            }
            catch
            {

            }
            finally
            {
                sqlConn.Close();
            }

            return false;
        }

        public bool createAirport(string aerodromID, string ime, string grad)
        {
            SqlConnection sqlConn = new SqlConnection(CONNECTION_STRING);
            SqlCommand sqlComm = new SqlCommand();
            sqlComm = sqlConn.CreateCommand();
            sqlComm.CommandText = @"INSERT INTO aerodromi (aerodrom_id, ime, grad) VALUES (@aerodrom_id, @ime, @grad)";
            sqlComm.Parameters.Add("@aerodrom_id", SqlDbType.VarChar);
            sqlComm.Parameters["@aerodrom_id"].Value = aerodromID;
            sqlComm.Parameters.Add("@ime", SqlDbType.VarChar);
            sqlComm.Parameters["@ime"].Value = ime;
            sqlComm.Parameters.Add("@grad", SqlDbType.VarChar);
            sqlComm.Parameters["@grad"].Value = grad;

            try
            {
                sqlConn.Open();
                sqlComm.ExecuteNonQuery();
                return true;
            }
            catch
            {

            }
            finally
            {
                sqlConn.Close();
            }

            return false;
        }

        public bool addNewCompany(string ime, string telefon)
        {
            SqlConnection sqlConn = new SqlConnection(CONNECTION_STRING);
            SqlCommand sqlComm = new SqlCommand();
            sqlComm = sqlConn.CreateCommand();
            sqlComm.CommandText = @"INSERT INTO prevozioci (telefon, ime, status) VALUES (@telefon, @ime, @status)";
            sqlComm.Parameters.Add("@telefon", SqlDbType.VarChar);
            sqlComm.Parameters["@telefon"].Value = telefon;
            sqlComm.Parameters.Add("@ime", SqlDbType.VarChar);
            sqlComm.Parameters["@ime"].Value = ime;
            sqlComm.Parameters.Add("@status", SqlDbType.Int);
            sqlComm.Parameters["@status"].Value = 1;

            try
            {
                sqlConn.Open();
                sqlComm.ExecuteNonQuery();
                return true;
            }
            catch
            {

            }
            finally
            {
                sqlConn.Close();
            }

            return false;
        }

        public bool addNewAircraft(string avionID, string brojSedista, string prevozilacID)
        {
            SqlConnection sqlConn = new SqlConnection(CONNECTION_STRING);
            SqlCommand sqlComm = new SqlCommand();
            sqlComm = sqlConn.CreateCommand();
            sqlComm.CommandText = @"INSERT INTO avioni (avion_id, broj_sedista, prevozilac_id) VALUES (@avionID, @brojSedista, @prevozilacID)";
            sqlComm.Parameters.Add("@avionID", SqlDbType.VarChar);
            sqlComm.Parameters["@avionID"].Value = avionID;
            sqlComm.Parameters.Add("@brojSedista", SqlDbType.Int);
            sqlComm.Parameters["@brojSedista"].Value = brojSedista;
            sqlComm.Parameters.Add("@prevozilacID", SqlDbType.Int);
            sqlComm.Parameters["@prevozilacID"].Value = prevozilacID;

            try
            {
                sqlConn.Open();
                sqlComm.ExecuteNonQuery();
                return true;
            }
            catch
            {

            }
            finally
            {
                sqlConn.Close();
            }

            return false;
        }

        public bool makeReservation(string brojSedista, string klasa, string jmbg, string letID)
        {
            SqlConnection sqlConn = new SqlConnection(CONNECTION_STRING);
            SqlCommand sqlComm = new SqlCommand();
            sqlComm = sqlConn.CreateCommand();
            sqlComm.CommandText = @"INSERT INTO rezervacije (kreirano_dana, broj_sedista, klasa, jmbg, let_id, status) VALUES (@kreiranoDana, @brojSedista, @klasa, @jmbg, @letID, @status)";
            sqlComm.Parameters.Add("@kreiranoDana", SqlDbType.DateTime);
            sqlComm.Parameters["@kreiranoDana"].Value = DateTime.Now;
            sqlComm.Parameters.Add("@brojSedista", SqlDbType.Int);
            sqlComm.Parameters["@brojSedista"].Value = brojSedista;
            sqlComm.Parameters.Add("@klasa", SqlDbType.Int);
            sqlComm.Parameters["@klasa"].Value = klasa;
            sqlComm.Parameters.Add("@jmbg", SqlDbType.VarChar);
            sqlComm.Parameters["@jmbg"].Value = jmbg;
            sqlComm.Parameters.Add("@letID", SqlDbType.VarChar);
            sqlComm.Parameters["@letID"].Value = letID;
            sqlComm.Parameters.Add("@status", SqlDbType.Int);
            sqlComm.Parameters["@status"].Value = 1;

            try
            {
                sqlConn.Open();
                sqlComm.ExecuteNonQuery();
                return true;
            }
            catch
            {

            }
            finally
            {
                sqlConn.Close();
            }

            return false;
        }
    }
}
