﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAKSR
{
    class ClassKorisnik
    {
        public static string jmbgS = "";
        public static string imeS = "";
        public static string prezimeS = "";
        public static int statusS = 0;
        public static int privilegijaS = 0;
        public static DateTime datumRodjenjaS = DateTime.Now;
        public static string korisnickoImeS = "";
        public static string emailS = "";

        public ClassKorisnik()
        {
            this.jmbg = "";
            this.ime = "";
            this.prezime = "";
            this.status = 0;
            this.privilegija = 0;
            this.datumRodjenja = DateTime.Now;
            this.korisnickoIme = "";
            this.email = "";
        }

        public ClassKorisnik(string jmbg, string ime, string prezime, int status, int privilegija, DateTime datumRodjenja, string korisnickoIme, string email)
        {
            this.jmbg = jmbg;
            this.ime = ime;
            this.prezime = prezime;
            this.status = status;
            this.privilegija = privilegija;
            this.datumRodjenja = datumRodjenja;
            this.korisnickoIme = korisnickoIme;
            this.email = email;
        }

        public string jmbg { get; set; }
        public string ime { get; set; }
        public string prezime { get; set; }
        public int status { get; set; }
        public int privilegija { get; set; }
        public DateTime datumRodjenja { get; set; }
        public string korisnickoIme { get; set; }
        public string email { get; set; }

        public static void resetKorisnik () {
            jmbgS = "";
            imeS = "";
            prezimeS = "";
            statusS = 0;
            privilegijaS = 0;
            datumRodjenjaS = DateTime.Now;
            korisnickoImeS = "";
            emailS = "";
        }
    }
}
