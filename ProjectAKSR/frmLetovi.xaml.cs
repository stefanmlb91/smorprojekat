﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectAKSR
{
    /// <summary>
    /// Interaction logic for frmLetovi.xaml
    /// </summary>
    public partial class frmLetovi : Window
    {
        AKSREntities context = new AKSREntities();
        letovi odabranLet = new letovi();

        public frmLetovi()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            ucitajLetove();

            lvLetovi.SelectedItem = null;

            if (ClassKorisnik.privilegijaS == 1)
            {
                buttonKorisnici.Visibility = Visibility.Visible;
                buttonUnesiLet.Visibility = Visibility.Visible;
                buttonDodajAvion.Visibility = Visibility.Visible;
                buttonDodajPrevozioca.Visibility = Visibility.Visible;
                buttonKorisnici.Visibility = Visibility.Visible;
                buttonUnesiAerodrom.Visibility = Visibility.Visible;
                buttonIzmeniLet.Visibility = Visibility.Visible;
                buttonIzmeniLet.IsEnabled = false;
            }
            else
            {
                buttonKorisnici.Visibility = Visibility.Hidden;
                buttonUnesiLet.Visibility = Visibility.Hidden;
                buttonDodajAvion.Visibility = Visibility.Hidden;
                buttonDodajPrevozioca.Visibility = Visibility.Hidden;
                buttonKorisnici.Visibility = Visibility.Hidden;
                buttonUnesiAerodrom.Visibility = Visibility.Hidden;
                buttonIzmeniLet.Visibility = Visibility.Hidden;
            }
        }
        
        private void ucitajLetove()
        {
            /*
            var flights = from flight in context.letovis
                          join carrier in context.prevoziocis on flight.prevozilac_id equals carrier.prevozilac_id
                          select new
                          {
                              flight.let_id,
                              flight.prevozilac_id,
                              flight.avion_id,
                              flight.aerodrom_polaska_id,
                              flight.aerodrom_dolaska_id,
                              flight.datum_leta,
                              flight.status,
                              carrier.ime
                          };
            lvLetovi.ItemsSource = flights.ToArray();
             * */
            lvLetovi.ItemsSource = listaLetova();
        }

        private ObservableCollection<letovi> listaLetova()
        {
            var list = from e in context.letovis select e;
            return new ObservableCollection<letovi>(list);
        }

        private void buttonUnesiLet_Click(object sender, RoutedEventArgs e)
        {
            frmUnesiLet noviLet = new frmUnesiLet();
            noviLet.ShowDialog();
        }

        private void buttonUnesiAerodrom_Click(object sender, RoutedEventArgs e)
        {
            frmUnesiAerodrom frmNoviAerodrom = new frmUnesiAerodrom();
            frmNoviAerodrom.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviAerodrom.ShowDialog();
        }

        private void buttonDodajPrevozioca_Click(object sender, RoutedEventArgs e)
        {
            frmUnesiPrevozioca frmNoviPrevozilac = new frmUnesiPrevozioca();
            frmNoviPrevozilac.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviPrevozilac.ShowDialog();
        }

        private void buttonDodajAvion_Click_1(object sender, RoutedEventArgs e)
        {
            frmUnesiAvion frmNoviAvion = new frmUnesiAvion();
            frmNoviAvion.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviAvion.ShowDialog();
        }

        private void buttonKorisnici_Click(object sender, RoutedEventArgs e)
        {
            frmKorisnici frmKorisnici = new frmKorisnici();
            frmKorisnici.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmKorisnici.ShowDialog();
        }

        private void buttonIzadji_Click(object sender, RoutedEventArgs e)
        {
            ClassKorisnik.resetKorisnik();
            frmLogin frmLogovanje = new frmLogin();
            frmLogovanje.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmLogovanje.Show();
            this.Close();
        }

        private void lvLetovi_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void lvLetovi_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            buttonIzmeniLet.IsEnabled = true;
            var let = lvLetovi.SelectedItem;
        }

        private void buttonIzmeniLet_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void lvLetovi_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void buttonRezervisi_Click(object sender, RoutedEventArgs e)
        {
            var let = (letovi)lvLetovi.SelectedItem;
            rezervacije rezervacija = new rezervacije();

            if (let == null)
            {
                MessageBox.Show("Morate prvo odabrati let.");
            }
            else
            {
                frmRezervisiLet frmRez = new frmRezervisiLet(let);
                frmRez.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
                frmRez.ShowDialog();
            }
        }
    }
}
